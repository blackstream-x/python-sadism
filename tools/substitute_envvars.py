#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

substitute_envvars.py

Substitute certain environ variables in the provided file,
except ome forbitten keys (to prevent secrets leakage)

"""


import argparse
import os
import string
import sys

from typing import Dict


FORBIDDEN_KEYS = (
    "CI_JOB_JWT",
    "CI_REPOSITORY_URL",
    "PIP_INDEX_URL",
    "TOKEN",
    "PASSWORD",
)


def __get_arguments() -> argparse.Namespace:
    """Parse command line arguments"""
    argument_parser = argparse.ArgumentParser(
        description="Substitute environment variables in a file"
    )
    argument_parser.add_argument(
        "target_file",
        help="The target file for the substitute operation",
    )
    return argument_parser.parse_args()


def main(arguments: argparse.Namespace) -> int:
    """Main script function"""
    # Build substitutions dict
    substitutions: Dict[str, str] = {}
    for (key, value) in os.environ.items():
        if key in FORBIDDEN_KEYS:
            continue
        #
        substitutions[key] = value
    #
    with open(arguments.target_file, mode="r", encoding="utf-8") as source:
        source_template = string.Template(source.read())
    #
    target_content: str = source_template.safe_substitute(substitutions)
    with open(arguments.target_file, mode="w", encoding="utf-8") as target:
        target.write(target_content)
    #
    return 0


if __name__ == "__main__":
    sys.exit(main(__get_arguments()))


#
