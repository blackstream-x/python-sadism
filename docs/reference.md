# Reference

## Module contents

### Exceptions

sadism.**NoResultsError**

> Raised if a search returned no results

sadism.**TooManyResultsError**

> Raised if a search returned more results than expected

### Class

#### sadism.**Connector**(*bind_dn=None*, *bind_password=None*, *cacerts_file=None*)

This is a base class for Active Direcory resp. LDAP connectors.
To use it, you should subclass it and overwrite the following class attributes
in your subclass.

Instances are initialized with the DN and password
(Parameters **bind\_dn** and **bind\_password**) of a bind user,
ie. a special user that is used to bind to Active Directory.
The **cacerts\_file** is the relative or absolute path of a file
containing CA certificates which will be used to verify the
certificate of the server when using an `ldaps://` URL with a private CA.

##### Class attributes

###### .ldap_url

URL of the LDAP server (`ldap://...` or `ldaps://...`), preset to `ldaps://ldap.example.com:3269`.

###### .user_search_base_dn

Base DN of the directory for user searches, preset to `DC=example,DC=com`.

###### .user_search_attribute

Attribute representing the user ID, preset to `uid`

###### .prx_user_id

Regular Expression tor user ID sanitization, preset to `^([a-z0-9]+)`.

The first match group will be returned by the **.sanitized()** method.

###### .search_timeout

Default timeout (in seconds) for LDAP searches, preset to `2`.

###### .attributes_timeout

Default timeout (in seconds) for attribute retrieval, preset to `1`.

##### Instance methods

###### .set_bind_user(_user\_dn=None, password=None_)

> Use the provided DN and password for the following LDAP bind operations.
> If _user\_dn_ is set to `None`,
> use the internally stored credentials of the bind user.

###### .bind_and_search(_*attrs, base=None, scope=ldap.SCOPE\_SUBTREE, filterstr=`"(objectClass=*)"`, timeout=None_)

> Bind using the credentials set by the **.set_bind_user()** method,
> then perform an LDAP search operation using
> [ldap.LDAPObject.search_st()](https://www.python-ldap.org/en/python-ldap-3.4.0/reference/ldap.html#ldap.LDAPObject.search_st)
> and return the results as a list.
> The parameters for this method follow the same semantics as in the wrapped
> function.
> **\*attrs** are the names of attributes to be included in each result member.
> **base** is the DN of the entry at which to start the search, defaulting to
> the value of the class attribute **.user\_search\_base\_dn**.
> **scope** is the scope of the query. If the bind is performed using
> a different DN than the internally stored DN of the bind user,
> scope will be modified to ldap.SCOPE\_BASE, so regular users cannot
> search through the whole tree.
> **filterstr** is the filter string for the query.
> **timeout** defines a timeout in seconds, defaulting to the value of
> the class attribute **.search\_timeout**. Setting it to `-1` disables the timeout.

> The returned list is a list of (dn, attrs) tuples where dn is the
> DN of each found LDAP entry, and attrs is a dict of attributes,
> with the attribute name as the key and a list of attribute contents
> a the value for each attribute.

###### .sanitized(_user\_id_)

> Returns the part of **user_id** that matches first group
> in the class attribute **.prx_user_id**.

###### .get_own_attributes(_user\_dn, password, \*attrs, timeout=None_)

> Bind with **user\_dn** and **password** and return a dict containing
> all found attributes of **attrs** in the LDAP entry identified by **user\_dn**.
> **timeout** defines the timeout as documented for **.bind\_and\_search()**,
> and defaults to the value of the class attribute **.attributes\_timeout**.

###### .get_user_attributes(_user\_id, password, \*attrs, search\_timeout=None, attributes\_timeout=None_)

> Search for the user's DN first, then bind with the found DN
> and **password**, and finally return a dict containing
> all found attributes of **attrs** in the LDAP entry identified by **user\_dn**.
> **search\_timeout** defines the timeout for the user DN search
> and defaults to the value of the class attribute **.search\_timeout**.
> **attributes\_timeout** defines the timeout for the attributes retrieval
> and defaults to the value of the class attribute **.attributes\_timeout**.
> Each of these timeouts may be disabled by setting it to `-1`.


