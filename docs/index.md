# Simplified Active Directory Information Search Mechanism

## Installation

```
pip install sadism
```

## Module usage

This module provides one base class for simplified search in Active Directory.
To customize it to your Active Directory installation,
define a subclass overwriting the following attributes:

- `ldap_url`
- `user_search_base_dn`
- `user_search_attribute`
- `prx_user_id`
- `search_timeout`
- `attributes_timeout`

See the _Reference_ page for details on usage of the class.

